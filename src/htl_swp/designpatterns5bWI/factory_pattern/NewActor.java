package htl_swp.designpatterns5bWI.factory_pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class NewActor implements Actor {

    private double x,y;

    public NewActor(double x, double y) {
        super();
        this.x = x;
        this.y = y;
    }

    @Override
    public void update(GameContainer gc, int delta) {

    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawString("I'm a new Actor", (float)this.x, (float)this.y);
    }
}
