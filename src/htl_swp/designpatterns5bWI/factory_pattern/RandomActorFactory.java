package htl_swp.designpatterns5bWI.factory_pattern;

import java.util.Random;

public class RandomActorFactory {
    public static Actor getRandomActor() {
        Random random = new Random();
        int number = random.nextInt(3);

        if (number==0) {
            return new Circle(random.nextInt(500), random.nextInt(500));
        }
        if (number==1) {
            return new NewActor(random.nextInt(700), random.nextInt(700));
        }

        return new NewActor(random.nextInt(700), random.nextInt(700));
    }

}
