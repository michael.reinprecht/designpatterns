package htl_swp.designpatterns5bWI.adapter_pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class LegacyCircle implements LegacyActor {
    int x;
    @Override
    public void move(GameContainer gc) {
        this.x -= 1;
    }

    @Override
    public void draw(Graphics graphics, int delta) {
        graphics.drawOval(50,50,50,50);
        System.out.println("I wonder what to do with delta, let's just print it" + delta);
    }
}
