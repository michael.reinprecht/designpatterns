package htl_swp.designpatterns5bWI.adapter_pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface LegacyActor {
    void move(GameContainer gc);
    void draw(Graphics graphics, int delta);
}
