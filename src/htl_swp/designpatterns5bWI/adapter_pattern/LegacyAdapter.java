package htl_swp.designpatterns5bWI.adapter_pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class LegacyAdapter implements Actor {

    LegacyActor legacyActor;
    int delta;

    public LegacyAdapter(LegacyActor legacyActor) {
        this.legacyActor = legacyActor;
    }

    @Override
    public void update(GameContainer gc, int delta) {
        this.legacyActor.move(gc);
        this.delta = delta;
    }

    @Override
    public void render(Graphics graphics) {
        this.legacyActor.draw(graphics,this.delta);
    }
}
