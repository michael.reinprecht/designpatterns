package htl_swp.designpatterns5bWI.observer_pattern;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Rectangle extends AbstractActor implements Observer {
    private Color color;

    public Rectangle(MoveStrategy moveStrategy) {
        super(moveStrategy);
        this.color = Color.white;
    }

    public void render(Graphics g) {
        g.setColor(this.color);
        g.fillRect(this.moveStrategy.getX(), this.moveStrategy.getY(), 40, 40);
        g.setColor(Color.white);
    }

    @Override
    public void inform() {
        this.color = Color.pink;
    }
}
