package htl_swp.designpatterns5bWI.observer_pattern;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Circle extends AbstractActor implements Observer {
    private Color color = Color.white;

    public Circle(MoveStrategy moveStrategy) {
        super(moveStrategy);
    }

    public void render(Graphics g) {
        g.setColor(this.color);
        g.fillOval(this.moveStrategy.getX(), this.moveStrategy.getY(), 60, 60);
        g.setColor(Color.white);
    }

    public void inform() {
        this.color = Color.blue;
    }
}


