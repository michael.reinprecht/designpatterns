package htl_swp.designpatterns5bWI.observer_pattern;

public interface Observer {
    public void inform();
}
