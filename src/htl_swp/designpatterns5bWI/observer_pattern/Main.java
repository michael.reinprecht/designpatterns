package htl_swp.designpatterns5bWI.observer_pattern;

import org.newdawn.slick.*;

import java.util.ArrayList;
import java.util.List;

public class Main extends BasicGame {


    private List<Actor> actors;


    public Main(String title) {
        super(title);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        for (Actor actor: this.actors) {
            actor.render(graphics);
        }
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.actors = new ArrayList<>();
        Player player = new Player(200,200);

        MoveStrategy movementC1 = new MoveRight(50,20,0.1f);
        MoveStrategy movementC2 = new MoveRight(50,100,0.05f);
        MoveStrategy movementR1 = new MoveRight(100, 200, 0.05f);

        Circle playercircle = new Circle(movementC1);
        Circle playercircle2 = new Circle(movementC2);
        Rectangle playerrectangle = new Rectangle(movementR1);

        this.actors.add(playercircle);
        this.actors.add(playercircle2);
        this.actors.add(playerrectangle);

        this.actors.add(new NewActor());
        this.actors.add(player);

        player.addObserver(playercircle);
        player.addObserver(playercircle2);
        player.addObserver(playerrectangle);
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        for (Actor actor: this.actors) {
            actor.update(gameContainer, delta);
        }
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new Main("Add_Object"));
            container.setDisplayMode(800,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}

