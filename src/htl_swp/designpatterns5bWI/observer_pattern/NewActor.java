package htl_swp.designpatterns5bWI.observer_pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class NewActor implements Actor {

    @Override
    public void update(GameContainer gc, int delta) {

    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawString("I'm a new Actor", 50, 50);
    }
}
