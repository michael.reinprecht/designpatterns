package htl_swp.designpatterns5bWI.strategy_pattern;

import org.newdawn.slick.Graphics;

public class MoveLeft implements  MoveStrategy {
    private float x,y,speed;

    public MoveLeft(float x, float y, float speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    public void update(int delta) {
        this.x -= delta * speed;
    }

    public void render(Graphics graphics) {
        graphics.drawOval(50,50,50,50);
    }


    //Getters and Setters
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
