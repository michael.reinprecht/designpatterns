package htl_swp.designpatterns5bWI.strategy_pattern;

import org.newdawn.slick.*;

import java.util.ArrayList;
import java.util.List;

public class Main extends BasicGame {


    private List<Actor> actors;


    public Main(String title) {
        super(title);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        for (Actor actor: this.actors) {
            actor.render(graphics);
        }
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.actors = new ArrayList<>();

        MoveStrategy movementC1 = new MoveRight(50,20,0.3f);
        MoveStrategy movementC2 = new MoveLeft(250,100,0.2f);
        MoveStrategy movementR1 = new MoveRight(100, 150, 0.4f);

        this.actors.add(new Circle(movementC1));
        this.actors.add(new Circle(movementC2));
        this.actors.add(new Rectangle(movementR1));
        this.actors.add(new NewActor());
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        for (Actor actor: this.actors) {
            actor.update(gameContainer, delta);
        }
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new Main("Add_Object"));
            container.setDisplayMode(800,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}

