package htl_swp.designpatterns5bWI.strategy_pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Rectangle implements Actor {
    private MoveStrategy movement;

    public Rectangle(MoveStrategy movement) {
        super();
        this.movement = movement;

    }

    public void update(GameContainer gc, int delta) {
        movement.update(delta);
    }

    public void render(Graphics graphics) {
        graphics.drawRect((float)movement.getX(), (float)movement.getY(), 40, 40);
    }
}
