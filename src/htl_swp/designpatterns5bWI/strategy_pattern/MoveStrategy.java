package htl_swp.designpatterns5bWI.strategy_pattern;

public interface MoveStrategy {
    public float getX();
    public float getY();
    public void update(int delta);
}
