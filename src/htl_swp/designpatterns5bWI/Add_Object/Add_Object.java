package htl_swp.designpatterns5bWI.Add_Object;

import org.newdawn.slick.*;

public class Add_Object extends BasicGame {

    private Circle c1;


    public Add_Object(String title) {
        super(title);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        this.c1.render(graphics);
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.c1 = new Circle(100,100);
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
                this.c1.update(gameContainer, delta);
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new Add_Object("Add_Object"));
            container.setDisplayMode(800,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}

