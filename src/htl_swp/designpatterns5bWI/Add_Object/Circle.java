package htl_swp.designpatterns5bWI.Add_Object;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Circle {
    private double x,y;

    public Circle(double x, double y) {
        super();
        this.x = x;
        this.y = y;
    }

    public void update(GameContainer gc, int delta) {
        this.x += delta;
    }

    public void render(Graphics graphics) {
        graphics.drawOval((float)this.x, (float)this.y, 20, 20);
    }
}
