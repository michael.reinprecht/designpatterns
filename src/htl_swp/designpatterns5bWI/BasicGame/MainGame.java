package htl_swp.designpatterns5bWI.BasicGame;
import org.newdawn.slick.*;


public class MainGame extends BasicGame {

    private int rectx, recty;


    public MainGame(String title) {
        super(title);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        graphics.drawRect(this.rectx, this.recty,50,50);
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.rectx = 100;
        this.recty = 100;
    }

    @Override
    public void update(GameContainer gameContainer, int i) throws SlickException {

    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new MainGame("BasicGame"));
            container.setDisplayMode(800,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
