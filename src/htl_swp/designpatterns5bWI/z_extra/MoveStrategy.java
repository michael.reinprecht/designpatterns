package htl_swp.designpatterns5bWI.z_extra;

public interface MoveStrategy {
    float getX();
    float getY();
    void update(int delta);
}
