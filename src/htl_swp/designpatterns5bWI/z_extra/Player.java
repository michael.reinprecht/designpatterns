package htl_swp.designpatterns5bWI.z_extra;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import java.util.ArrayList;
import java.util.List;

public class Player implements Actor {
    private double x,y;
    private List<Observer> observers;

    public Player(double x, double y) {
        super();
        this.x = x;
        this.y = y;
        this.observers = new ArrayList<>();
    }

    public void update(GameContainer gc, int delta) {
        if (gc.getInput().isKeyDown(Input.KEY_D)) {
            this.x += delta * 0.4;
        }
        if (gc.getInput().isKeyDown(Input.KEY_A)) {
            this.x -= delta * 0.4;
        }
        if (this.x > 700) {
            for (Observer observer : observers) {
                observer.inform();
            }
        }
    }

    public void render(Graphics graphics) {
        graphics.fillRect((float)this.x, (float)this.y, 80, 80);
    }

    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }
}
