package htl_swp.designpatterns5bWI.z_extra;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actor {
    void update(GameContainer gc, int delta);
    void render(Graphics graphics);
}
