package htl_swp.designpatterns5bWI.z_extra;



import java.util.Random;

public class RandomActorFactory {
    public static Actor getRandomActor(MoveStrategy moveStrategy) {
        Random random = new Random();
        int number = random.nextInt(3);

        return new Circle(moveStrategy);
    }

}
