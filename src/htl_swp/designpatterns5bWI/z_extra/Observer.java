package htl_swp.designpatterns5bWI.z_extra;

public interface Observer {
    void inform();
}
