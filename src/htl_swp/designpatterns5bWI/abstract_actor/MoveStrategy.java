package htl_swp.designpatterns5bWI.abstract_actor;

public interface MoveStrategy {
    public float getX();
    public float getY();
    public void update(int delta);
}
