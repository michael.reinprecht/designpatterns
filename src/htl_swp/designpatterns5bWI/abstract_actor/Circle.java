package htl_swp.designpatterns5bWI.abstract_actor;

import org.newdawn.slick.Graphics;

public class Circle extends AbstractActor {

    public Circle(MoveStrategy moveStrategy) {
        super(moveStrategy);
    }

    public void render(Graphics g) {
        g.drawOval(this.moveStrategy.getX(), this.moveStrategy.getY(), 10, 20);
    }
   }
