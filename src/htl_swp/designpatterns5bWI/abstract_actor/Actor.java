package htl_swp.designpatterns5bWI.abstract_actor;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actor {
    public void update(GameContainer gc, int delta);
    public void render(Graphics graphics);
}
