package htl_swp.designpatterns5bWI.abstract_actor;

import org.newdawn.slick.Graphics;

public class MoveRight implements MoveStrategy {
    private float x,y,speed;

    public MoveRight(float x, float y, float speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    public void update(int delta) {
        this.x += delta * speed;
    }

    public void render(Graphics graphics) {
        graphics.drawOval(50,50,50,50);
    }


    //Getters and Setters
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
