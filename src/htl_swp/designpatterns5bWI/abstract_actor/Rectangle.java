package htl_swp.designpatterns5bWI.abstract_actor;

import org.newdawn.slick.Graphics;

public class Rectangle extends AbstractActor {

    public Rectangle(MoveStrategy moveStrategy) {
        super(moveStrategy);
    }

    public void render(Graphics g) {
        g.drawRect(this.moveStrategy.getX(), this.moveStrategy.getY(), 10, 20);
    }
}
