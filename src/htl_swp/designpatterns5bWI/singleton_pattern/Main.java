package htl_swp.designpatterns5bWI.singleton_pattern;

import org.newdawn.slick.*;

public class Main extends BasicGame {

    public Main(String title) {
        super(title);
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {

    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        CounterSingleton cs1 = CounterSingleton.getInstance();
        CounterSingleton cs2 = CounterSingleton.getInstance();

        cs1.increaseCounter();
        cs2.increaseCounter();
        cs1.increaseCounter();
        cs2.increaseCounter();

        System.out.println(cs1.getCounter());
        System.out.println(cs2.getCounter());
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {

    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new Main("Add_Object"));
            container.setDisplayMode(800,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}

