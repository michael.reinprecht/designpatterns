package htl_swp.coding_contest3_MarsRover2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String input = "1.00 1.00 -15";
        Double r;
        Double degree;
        Double xPosition;
        Double yPosition;
        String output = "";
        String[] inputs = input.split(" ");
        List<Double> numbers = new ArrayList<Double>();

        for (int i = 0; i<inputs.length; i++) {
             numbers.add(Double.parseDouble(inputs[i]));
        }

        Double WheelBase = numbers.get(0);
        Double Distance = numbers.get(1);
        Double SteeringAngle = numbers.get(2);

        r = WheelBase / Math.sin(Math.toRadians(SteeringAngle));

        if (SteeringAngle == 0) {
            xPosition = 0.00;
            yPosition = Distance;
            degree = 0.00;
        }
        else {
            degree = (Distance*360) / (2* Math.PI * r);
            xPosition = r - Math.cos(Math.toRadians(degree)) * r;
            yPosition = Math.sin(Math.toRadians(degree)) * r;

            while (degree <0) {
                degree += 360;
            }
        }

        output = xPosition + " " + yPosition + " " + degree;
        System.out.println(output);
    }
}
