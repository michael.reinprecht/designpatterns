package htl_swp.designpatterns4bWI.hangman;

import java.util.Random;
import java.util.Scanner;


public class HangMan {

    public static void main(String[] args) {
        Random random = new Random();
        String[] words = new String[10];
        words[0] = "online";
        words[1] = "unterricht";
        words[2] = "quarantäne";
        words[3] = "corona";
        words[4] = "virus";
        words[5] = "ausgangssperre";
        words[6] = "china";
        words[7] = "italien";
        words[8] = "amerika";
        words[9] = "österreich";

        String word = words[random.nextInt(9)];
        String secret = "";

        String[] letters = new String[word.length()];
        String[] secretletters = new String[word.length()];

        startUp(word, letters, secretletters, random);




        Scanner scanner = new Scanner(System.in);
        String input;


        while (true) {
            secret = "";
            for (int i = 0; i<secretletters.length; i++) {
                secret+=secretletters[i];
            }
            Print(secret);
            input = scanner.nextLine();
            chooseALetter(input, scanner, secretletters, letters, random, word);
        }

    }




    public static void chooseALetter(String input, Scanner scanner, String[] secretletters, String[] letters, Random random, String word) {
        boolean trueawnser = false;
        int randomnumber;

        for (int i = 0; i < secretletters.length; i++) {
            if (letters[i] == input) {
                System.out.println("Richtig!");
                secretletters[i] = input;
                trueawnser = true;
            }
        }
        while (trueawnser == false) {
            randomnumber = random.nextInt(word.length());
            if (secretletters[randomnumber] == "*") {
                trueawnser = true;
            }
            secretletters[randomnumber] = letters[randomnumber];
        }
    }

    public static void Print(String secret) {
        System.out.println("Das zu erratende Wort: " + secret);
        System.out.println("Wählen Sie einen Buchstaben:");
    }

    public static void startUp(String word, String[] letters, String[] secretletters, Random random) {
        int randomnumber = random.nextInt(word.length());

        for (int i = 0; i < word.length(); i++) {
            letters[i] = word.substring(i,i+1);
            secretletters[i] = word.substring(i, i+1);
        }


        for (int i = 0; i < 4; i++) {
            if (secretletters[randomnumber] == "*") {
                i--;
                randomnumber = random.nextInt(word.length());
            }
            else {
                secretletters[randomnumber] = "*";
            }
        }
    }
}
