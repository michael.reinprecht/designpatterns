package htl_swp.designpatterns4bWI.refactoring;

import org.newdawn.slick.*;

public class ObjectsGame extends BasicGame {
    private Rectangle rectangle;
    private Rectangle rectangle2;

    public ObjectsGame(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gc) throws SlickException {
    this.rectangle = new Rectangle(100, 100, 1);
    this.rectangle2 = new Rectangle(100,200, 2);
    }

    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
    this.rectangle.update(delta);
    this.rectangle2.update(delta);
     }

    @Override
    public void render(GameContainer gc, Graphics graphics) throws SlickException {
    this.rectangle.render(graphics);
    this.rectangle2.render(graphics);
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new ObjectsGame("Aufgabe1"));
            container.setDisplayMode(800,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
