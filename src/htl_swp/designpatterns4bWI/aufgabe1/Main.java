package htl_swp.designpatterns4bWI.aufgabe1;

import org.newdawn.slick.*;

public class Main extends BasicGame {
    private int ovalx, ovaly, rectx, recty, circlex, circley;
    private boolean ovalLeft, ovalRight, circleUp, circleDown;

    public Main(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gc) throws SlickException {
        this.rectx = 100;
        this.recty = 100;
        this.ovalx = 150;
        this.circley = 150;
        this.ovalLeft = false;
        this.ovalRight = true;
    }

    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        if (this.rectx >= 90 && this.rectx < 700 && this.recty == 100) {
            this.rectx += delta;
        }
        if (this.recty >= 90 && this.recty < 410 && this.rectx >= 700) {
            this.recty += delta;
        }
        if (this.recty >= 400 && this.rectx <= 710 && this.rectx >= 100) {
            this.rectx -= delta;
        }
        if (this.rectx <= 100 && this.recty <= 410 && this.recty > 0) {
            this.recty -= delta;
        }

        if (this.ovalx <= 150) {
            this.ovalRight = true;
            this.ovalLeft = false;
        }
        if (this.ovalx >= 600) {
            this.ovalRight = false;
            this.ovalLeft = true;
        }
        if (this.ovalRight == true) {
            this.ovalx+= delta;
        }
        if (this.ovalLeft == true) {
            this.ovalx-=delta;
        }

        if (this.circley <= 150) {
            this.circleDown = true;
            this.circleUp = false;
        }
        if (this.circley >= 400) {
            this.circleDown = false;
            this.circleUp = true;
        }
        if (this.circleDown == true) {
            this.circley+= delta;
        }
        if (this.circleUp == true) {
            this.circley-=delta;
        }


     }

    @Override
    public void render(GameContainer gc, Graphics graphics) throws SlickException {
        graphics.drawRect(this.rectx, this.recty,50,50);
        graphics.drawOval(this.ovalx, this.ovaly, 150, 50);
        graphics.drawOval(this.circlex, this.circley, 100, 100);
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new Main("Aufgabe1"));
            container.setDisplayMode(800,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
