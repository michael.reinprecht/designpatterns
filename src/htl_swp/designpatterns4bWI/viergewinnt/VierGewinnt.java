package htl_swp.designpatterns4bWI.viergewinnt;

import java.util.Scanner;


public class VierGewinnt {

    public static void main(String[] args) {

        char[][] field = new char[6][7];
        for (int i = 0; i<6;i++) {
            for (int j = 0; j<7; j++) {
                field[i][j] = ' ';
            }
        }

        Scanner scanner = new Scanner(System.in);
        String input;
        String player = "Player1";

        while (true) {
            System.out.println("Make a turn " + player + "!");
            input = scanner.nextLine();

            //Zerlegt den Input, prüft ob das Feld besetzt ist, und ändert das Feld wenn nicht abhängig vom Spieler
            userINput(input, field, player, scanner);

            //Printet das neue Feld
            Print(field);

            //Überprüft ob und welcher Spieler gewonnen hat.
            checkIfWinner(player, field);

            //Wechselt jede Runde den Spieler
            if(player=="Player1") {
                player="Player2";
            }
            else {
                player="Player1";
            }
        }

    }



    public static void userINput(String inp, char[][] field, String player, Scanner scanner) {
        char empty = ' ';
        String[] input = inp.split(",");
        int line = Integer.parseInt(input[0]);
        int column = Integer.parseInt(input[1]);


        while (field[column][line] != empty) {
            if (field[column][line] != empty) {
                System.out.println("That field is already used! Please choose another!");
                inp = scanner.nextLine();
                input = inp.split(",");
                line = Integer.parseInt(input[0]);
                column = Integer.parseInt(input[1]);
            }
            else {
                if (player == "Player1") {
                    field[column][line] = 'X';
                } else {
                    field[column][line] = 'O';
                }
            }
        }

        if (field[column][line] == empty) {
            if (player == "Player1") {
                field[column][line] = 'X';
            } else {
                field[column][line] = 'O';
            }
        }
    }

    public static void Print(char[][] field) {
        System.out.println("");
        System.out.println(field[0][0]+ " | " +field[0][1]+ " | " +field[0][2] + " | " +field[0][3] + " | " +field[0][4] + " | " +field[0][5] + " | " +field[0][6]);
        System.out.println("---------------------------");
        System.out.println(field[1][0]+ " | " +field[1][1]+ " | " +field[1][2]+ " | " +field[1][3]+ " | " +field[1][4]+ " | " +field[1][5]+ " | " +field[1][6]);
        System.out.println("---------------------------");
        System.out.println(field[2][0]+ " | " +field[2][1]+ " | " +field[2][2]+ " | " +field[2][3]+ " | " +field[2][4]+ " | " +field[2][5]+ " | " +field[2][6]);
        System.out.println("---------------------------");
        System.out.println(field[3][0]+ " | " +field[3][1]+ " | " +field[3][2]+ " | " +field[3][3]+ " | " +field[3][4]+ " | " +field[3][5]+ " | " +field[3][6]);
        System.out.println("---------------------------");
        System.out.println(field[4][0]+ " | " +field[4][1]+ " | " +field[4][2]+ " | " +field[4][3]+ " | " +field[4][4]+ " | " +field[4][5]+ " | " +field[4][6]);
        System.out.println("---------------------------");
        System.out.println(field[5][0]+ " | " +field[5][1]+ " | " +field[5][2]+ " | " +field[5][3]+ " | " +field[5][4]+ " | " +field[5][5]+ " | " +field[5][6]);
        System.out.println("");
    }

    public static void checkIfWinner(String player, char[][] field) {

        //Player1
        for (int i = 0; i<3; i++) {
            for (int j = 0; j<7; j++) {
                if (field[i][j] == 'X' && field[i+1][j] == 'X' && field[i+2][j] == 'X' && field[i+3][j] == 'X') {
                    System.out.println("Player1 has won!");

                }
            }
        }

        for (int i = 0; i<6; i++) {
            for (int j = 0; j<4; j++) {
                if (field[i][j] == 'X' && field[i][j+1] == 'X' && field[i][j+2] == 'X' && field[i][j+3] == 'X') {
                    System.out.println("Player1 has won!");
                }
            }
        }

        for (int i = 0; i<3; i++) {
            for (int j = 0; j<4; j++) {
                if (field[i][j] == 'X' && field[i+1][j+1] == 'X' && field[i+2][j+2] == 'X' && field[i+3][j+3] == 'X') {
                    System.out.println("Player1 has won!");
                }
            }
        }

        for (int i = 0; i<3; i++) {
            for (int j = 3; j<7; j++) {
                if (field[i][j] == 'X' && field[i+1][j-1] == 'X' && field[i+2][j-2] == 'X' && field[i+3][j-3] == 'X') {
                    System.out.println("Player1 has won!");
                }
            }
        }


        //Player2
        for (int i = 0; i<3; i++) {
            for (int j = 0; j<7; j++) {
                if (field[i][j] == 'O' && field[i+1][j] == 'O' && field[i+2][j] == 'O' && field[i+3][j] == 'O') {
                    System.out.println("Player2 has won!");
                }
            }
        }

        for (int i = 0; i<6; i++) {
            for (int j = 0; j<4; j++) {
                if (field[i][j] == 'O' && field[i][j+1] == 'O' && field[i][j+2] == 'O' && field[i][j+3] == 'O') {
                    System.out.println("Player2 has won!");
                }
            }
        }

        for (int i = 0; i<3; i++) {
            for (int j = 0; j<4; j++) {
                if (field[i][j] == 'O' && field[i+1][j+1] == 'O' && field[i+2][j+2] == 'O' && field[i+3][j+3] == 'O') {
                    System.out.println("Player2 has won!");
                }
            }
        }

        for (int j = 6; j>3; j--) {
            for (int i = 5; i>3; i--) {
                if (field[i][j] == 'O' && field[i-1][j-1] == 'O' && field[i-2][j-2] == 'O' && field[i-3][j-3] == 'O') {
                    System.out.println("Player2 has won!");
                }
            }
        }
    }
}
