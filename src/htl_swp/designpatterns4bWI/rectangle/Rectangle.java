package htl_swp.designpatterns4bWI.rectangle;

import org.newdawn.slick.*;

public class Rectangle extends BasicGame {

    private double x;
    private double y;

    public Rectangle(String title) {
        super("Rectangle");
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.x = 100;
        this.y = 10;
    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        this.x+= delta *0.3;
        this.y+= delta *0.1;

        if (this.x > 400) {
            this.x=0;
            this.y=0;
        }
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
    graphics.drawRect((float)this.x, (float)this.y, 100, 100);
    graphics.drawOval(90,30, 100, 100);
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new Rectangle("Rectangle"));
            container.setDisplayMode(1000,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
