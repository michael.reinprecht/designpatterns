package htl_swp.designpatterns4bWI.lists;

import org.newdawn.slick.Graphics;

import java.util.Random;

public class Circle {
    private enum DIRECTION {RIGHT, DOWN, LEFT, UP};
    private int x;
    private int y;
    private float speed;
    private int diameter;
    private Random random = new Random();

    public Circle() {
        this.x = random.nextInt(600);
        this.y = random.nextInt(600);
        this.speed = random.nextInt(40) + 10;
        this.diameter = random.nextInt(50);
    }

    public void render(Graphics graphics) {
        graphics.drawOval(this.x,this.y,this.diameter, this.diameter);
    }

    public void update(int delta) {
        this.x -= delta*this.speed;
        if(this.x < 0) {
            this.x = 800;
        }
    }

}
