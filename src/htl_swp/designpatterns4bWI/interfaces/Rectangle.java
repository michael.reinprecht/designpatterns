package htl_swp.designpatterns4bWI.interfaces;

import org.newdawn.slick.Graphics;

public class Rectangle implements Actor {
    public enum DIRECTION {RIGHT, DOWN, LEFT, UP};
    private int x;
    private int y;
    private DIRECTION direction;

    public Rectangle(int x, int y, float speed, DIRECTION direction) {
        this.x = x;
        this.y = y;
        this.speed = speed + 1;
        this.direction = direction;
    }

    private float speed;

    public void render(Graphics graphics) {
    graphics.drawRect(this.x,this.y,20,20);
    }

    public void update(int delta) {
        if (direction == DIRECTION.RIGHT) {
            this.x += delta*this.speed/10;
            if(this.x > 800) {
                this.x = 0;
            }
        }

        if (direction == DIRECTION.LEFT) {
            this.x -= delta*this.speed;
            if(this.x < 0) {
                this.x = 800;
            }
        }

        if (direction == DIRECTION.UP) {
            this.y -= delta*this.speed;
            if(this.y < 0) {
                this.y = 600;
            }
        }

        if (direction == DIRECTION.DOWN) {
            this.y += delta*this.speed;
            if(this.y > 600) {
                this.y = 0;
            }
        }


    }

}
