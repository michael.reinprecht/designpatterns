package htl_swp.designpatterns4bWI.interfaces;

import org.newdawn.slick.Graphics;

public class Eclipse implements Actor {
    private int x,y;
    private float speed;

    public Eclipse(int x, int y) {
        this.x = x;
        this.y = y;
        this.speed=1;
    }
    @Override
    public void render(Graphics graphics) {
        graphics.drawOval(this.x, this.y, 100, 50);
    }

    @Override
    public void update(int delta) {
        this.x += delta*this.speed;
        this.y += delta*this.speed/1.5;
        if (this.x > 800) {
            this.x = 0;
        }
        if (this.y > 600) {
            this.y = 0;
        }
    }
}
