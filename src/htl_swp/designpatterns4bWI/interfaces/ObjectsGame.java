package htl_swp.designpatterns4bWI.interfaces;

import org.newdawn.slick.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ObjectsGame extends BasicGame {
    private List<Actor> actors;


    public ObjectsGame(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gc) throws SlickException {
        Random random = new Random();
        this.actors = new ArrayList<>();


        for (int i = 0; i < 10; i++) {
            Rectangle rectangle = new Rectangle(random.nextInt(600), random.nextInt(600), random.nextInt(5), Rectangle.DIRECTION.LEFT);
            this.actors.add(rectangle);
        }
        for (int i = 0; i < 10; i++) {
            Circle circle = new Circle();
            this.actors.add(circle);
        }
        for (int i = 0; i < 10; i++) {
            Eclipse eclipse = new Eclipse(random.nextInt(800),random.nextInt(600));
            this.actors.add(eclipse);
        }

    }

    @Override
    public void update(GameContainer gc, int delta) throws SlickException {
        for (Actor actor:this.actors) {
            actor.update(delta);
        }
     }

    @Override
    public void render(GameContainer gc, Graphics graphics) throws SlickException {
        for (Actor actor:this.actors) {
            actor.render(graphics);
        }
    }

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new ObjectsGame("Aufgabe1"));
            container.setDisplayMode(800,600,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
