package htl_swp.OO_4bWI.tictactoe;

import java.util.Scanner;


public class TicTacToeMain {


    public static void main(String[] args) {
        char[][] field = new char[3][3];
        field[0][0]=' ';
        field[1][0]=' ';
        field[2][0]=' ';
        field[0][1]=' ';
        field[1][1]=' ';
        field[2][1]=' ';
        field[0][2]=' ';
        field[1][2]=' ';
        field[2][2]=' ';

        Scanner scanner = new Scanner(System.in);
        String input;
        String player = "Player1";

        while (true) {
            System.out.println("Make a turn " + player + "!");
            input = scanner.nextLine();

            //Zerlegt den Input, prüft ob das Feld besetzt ist, und ändert das Feld wenn nicht abhängig vom Spieler
            userINput(input, field, player, scanner);

            //Printet das neue Feld
            Print(field);

            //Überprüft ob und welcher Spieler gewonnen hat.
            checkIfWinner(player, field);

            //Wechselt jede Runde den Spieler
            if(player=="Player1") {
                player="Player2";
            }
            else {
                player="Player1";
            }
        }

    }



    public static void userINput(String inp, char[][] field, String player, Scanner scanner) {
        char empty = ' ';
        String[] input = inp.split(",");
        int line = Integer.parseInt(input[0]);
        int column = Integer.parseInt(input[1]);


        while (field[column][line] != empty) {
            if (field[column][line] != empty) {
                System.out.println("That field is already used! Please choose another!");
                inp = scanner.nextLine();
                input = inp.split(",");
                line = Integer.parseInt(input[0]);
                column = Integer.parseInt(input[1]);
            }
            else {
                if (player == "Player1") {
                    field[column][line] = 'X';
                } else {
                    field[column][line] = 'O';
                }
            }
        }

        if (field[column][line] == empty) {
            if (player == "Player1") {
                field[column][line] = 'X';
            } else {
                field[column][line] = 'O';
            }
        }
    }

    public static void Print(char[][] field) {
        System.out.println("");
        System.out.println(field[0][0]+ " | " +field[0][1]+ " | " +field[0][2]);
        System.out.println("----------");
        System.out.println(field[1][0]+ " | " +field[1][1]+ " | " +field[1][2]);
        System.out.println("----------");
        System.out.println(field[2][0]+ " | " +field[2][1]+ " | " +field[2][2]);
        System.out.println("");
    }

    public static void checkIfWinner(String player, char[][] field) {

        if (
                (field[0][0] == 'X' && field[0][1] == 'X' && field[0][2] == 'X') ||
                (field[1][0] == 'X' && field[1][1] == 'X' && field[1][2] == 'X') ||
                (field[2][0] == 'X' && field[2][1] == 'X' && field[2][2] == 'X') ||
                (field[0][0] == 'X' && field[1][0] == 'X' && field[2][0] == 'X') ||
                (field[0][1] == 'X' && field[1][1] == 'X' && field[2][2] == 'X') ||
                (field[0][2] == 'X' && field[1][2] == 'X' && field[2][2] == 'X') ||
                (field[0][0] == 'X' && field[1][1] == 'X' && field[2][2] == 'X') ||
                (field[0][2] == 'X' && field[1][1] == 'X' && field[2][0] == 'X')
        ) {
            System.out.println("Player1 has won!");
        }

        if (
                (field[0][0] == 'O' && field[0][1] == 'O' && field[0][2] == 'O') ||
                (field[1][0] == 'O' && field[1][1] == 'O' && field[1][2] == 'O') ||
                (field[2][0] == 'O' && field[2][1] == 'O' && field[2][2] == 'O') ||
                (field[0][0] == 'O' && field[1][0] == 'O' && field[2][0] == 'O') ||
                (field[0][1] == 'O' && field[1][1] == 'O' && field[2][2] == 'O') ||
                (field[0][2] == 'O' && field[1][2] == 'O' && field[2][2] == 'O') ||
                (field[0][0] == 'O' && field[1][1] == 'O' && field[2][2] == 'O') ||
                (field[0][2] == 'O' && field[1][1] == 'O' && field[2][0] == 'O')
        ) {
            System.out.println("Player1 has won!");
        }
    }

}
