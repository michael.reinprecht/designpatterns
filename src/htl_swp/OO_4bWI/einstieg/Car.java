package htl_swp.OO_4bWI.einstieg;

import java.util.ArrayList;
import java.util.List;

public class Car {
    private int fuelConsumption;
    private String brand;
    private String serialNumber;
    private String color;

    private Engine engine;
    private Tank tank;

    private List<RearMirror> mirrors;
    private List<Tires> tires;

    //Constructor
    public Car(int fuelConsumption, String brand, String serialNumber, String color, Tank tank, Engine engine) {
        this.fuelConsumption = fuelConsumption;
        this.brand = brand;
        this.serialNumber = serialNumber;
        this.color = color;
        this.tank = tank;
        this.engine = engine;
        this.mirrors = new ArrayList<>();
        this.tires = new ArrayList<>();
    }

    //Methods
    public void drive(int speed) {
        engine.drive(speed);
        this.tank.setFuelAmount(this.tank.getFuelAmount() - this.fuelConsumption);
    }

    public void brake() {
        System.out.println("Ich bremse");
    }

    public void turboBoost() {
        if(this.tank.getFuelAmount() > 0.1*this.tank.getFuelTank()) {
            System.out.println("SuperBoostMode");
        }
        else {
            System.out.println("Not enough fuel to go to Superboost");
        }
    }

    public void honk(int amountofRepetitions) {
        for(int i = 0; i<amountofRepetitions; i++) {
            System.out.println("Tuuuut. Ich bin ein " + this.brand + " und habe " + this.engine.getHorsePower());
        }
    }

    public int getRemainingRange() {
        int remainingRange = this.tank.getFuelAmount()/this.fuelConsumption;
        return remainingRange;
    }

    public void addMirror(RearMirror mirror) {
        this.mirrors.add(mirror);
    }

    public void addTire(Tires tire) {
        this.tires.add(tire);
    }

    //Generated Getters and Setters
    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public List<RearMirror> getMirrors() {
        return mirrors;
    }

    public void setMirrors(List<RearMirror> mirrors) {
        this.mirrors = mirrors;
    }

    public List<Tires> getTires() {
        return tires;
    }

    public void setTires(List<Tires> tires) {
        this.tires = tires;
    }
}

