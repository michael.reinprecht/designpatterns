package htl_swp.OO_4bWI.einstieg;

public class Tank {
    private int fuelAmount;
    private int fuelTank;

    public Tank(int fuelAmount, int fuelTank) {
        this.fuelAmount = fuelAmount;
        this.fuelTank = fuelTank;
    }

    public int getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(int fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public int getFuelTank() {
        return fuelTank;
    }
}
