package htl_swp.OO_4bWI.einstieg;

public class Engine {
    public enum FUELTYPE{GASOLINE, DIESEL}
    private int horsePower;
    private FUELTYPE type; //Diesel or Gasoline

    //Constructor
    public Engine(int horsePower, FUELTYPE type) {
        this.horsePower = horsePower;
        this.type = type;
    }

    //Method, should amount should be 0<amount<100
    public void drive(int amount) {
        if(amount<100 && 0<amount) {
            System.out.println("The Motor is running with " + amount);
            System.out.println("The Car is driving at a speed of " + amount + "km/h");
        }
        else {
            System.out.println("Der Motor kann nur mit einem amount von 1 bis 100 betrieben werden.");
        }
    }


    //Getters
    public int getHorsePower() {
        return horsePower;
    }

    public FUELTYPE getType() {
        return type;
    }
}
