package htl_swp.OO_4bWI.einstieg;

public class RearMirror {
    private int size;
    private int position; //0 -> neutral, -10 -> to the left, +10 -> to the right

    //Constructor
    public RearMirror(int size, int position) {
        this.size = size;
        this.position = position;
    }

    //Generated Getters and Setters
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
