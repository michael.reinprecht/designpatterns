package htl_swp.OO_4bWI.einstieg;

public class Main {
    public static void main(String[] args) {
        //Creating new Tank
        Tank tank1 = new Tank(70, 200);

        //Creating new Engine
        Engine engine1 = new Engine(200, Engine.FUELTYPE.GASOLINE);

        //Creating new RearMirror
        RearMirror mirror1 = new RearMirror(100, 0);
        RearMirror mirror2 = new RearMirror(100, -10);

        //Creating new Tires
        Tires tire1 = new Tires(200, Tires.SEASONTYPE.WINTER);
        Tires tire2 = new Tires(200, Tires.SEASONTYPE.WINTER);
        Tires tire3 = new Tires(200, Tires.SEASONTYPE.WINTER);
        Tires tire4 = new Tires(200, Tires.SEASONTYPE.WINTER);

        //Creating new Car(fuelConsumption, brand, serialNumber, color, tank, engine)
        Car car1 = new Car(7, "Audi", "233rt",  "blue", tank1, engine1);

        //Adding mirrors and tires to the car
        car1.addMirror(mirror1);
        car1.addMirror(mirror2);
        car1.addTire(tire1);
        car1.addTire(tire2);
        car1.addTire(tire3);
        car1.addTire(tire4);
    }
}
