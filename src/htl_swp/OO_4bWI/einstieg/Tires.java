package htl_swp.OO_4bWI.einstieg;

public class Tires {
    enum SEASONTYPE{WINTER, SUMMER}
    private int size;
    private SEASONTYPE season; //Winter or summer

    //Constructor
    public Tires(int size, SEASONTYPE season) {
        this.size = size;
        this.season = season;
    }



    //Generated Getters and Setters
    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public SEASONTYPE getSeason() {
        return season;
    }

    public void setSeason(SEASONTYPE season) {
        this.season = season;
    }
}
