package htl_swp.OO_4bWI.remote;

public class Main {
        public static void main(String[] args) {

            Battery battery1 = new Battery(80);
            Battery battery2 = new Battery(60);
            Remote remote1 = new Remote(true, 70);
            remote1.addBattery(battery1);
            remote1.addBattery(battery2);

            System.out.println("This remote has " + remote1.getStatus() + "% power.");
        }
}
