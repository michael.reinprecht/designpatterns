package htl_swp.OO_4bWI.remote;

import java.util.ArrayList;
import java.util.List;

public class Remote {
    private boolean isOn;
    private int power;
    private List<Battery> batteries;

    public Remote(boolean isOn, int power) {
        this.isOn = isOn;
        this.power = power;
        this.batteries = new ArrayList<>();
    }

    public void addBattery(Battery battery) {
        this.batteries.add(battery);
    }


    public void turnOn() {
        isOn = true;
    }

    public void turnOff() {
        isOn = false;
    }

    public boolean isOn() {
        return isOn;
    }

    public void setOn(boolean on) {
        isOn = on;
    }

    public int getStatus() {
        return power;
    }
}
