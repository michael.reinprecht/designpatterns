package htl_swp.coding_MarsRover3;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String input = "1.09 3 1.00 15.00 1.00 0.00 1.00 -15.00";
        Double Radius;
        Double xPosition = 0.00;
        Double yPosition = 0.00;
        String output = "";
        String[] inputs = input.split(" ");
        List<Double> numbers = new ArrayList<Double>();

        for (int i = 0; i<inputs.length; i++) {
             numbers.add(Double.parseDouble(inputs[i]));
        }

        Double WheelBase = numbers.get(0);
        Integer ReAdjustments = numbers.get(1).intValue();

        Double[] distances = new Double[ReAdjustments];
        Double[] steeringAngles = new Double[ReAdjustments];
        Double SteeringAngle;
        Double currentDirection = 0.00;
        Double turnRadius = 0.00;



        for (int i = 0; i<ReAdjustments*2; i+=2) {
            distances[i/2] =(numbers.get(i+2));
            steeringAngles[i/2] = (numbers.get(i+3));
        }



        for (int i=0; i<ReAdjustments; i++) {
            SteeringAngle = steeringAngles[i];

            if (SteeringAngle == 0) {
                xPosition += Math.sin(Math.toRadians(currentDirection)) * distances[i];
                yPosition += Math.cos(Math.toRadians(currentDirection)) * distances[i];
                System.out.println(Math.sin(Math.toRadians(currentDirection)) * distances[i]);
            }

            else {
                Radius = WheelBase / Math.sin(Math.toRadians(SteeringAngle));
                turnRadius = (distances[i] * 360) / (2 * Math.PI * Radius);
                currentDirection += turnRadius;

                //Berechnung der X-Position
                xPosition += Math.sin(Math.toRadians(currentDirection)) * distances[i]-(Radius - Math.cos(Math.toRadians(turnRadius)) * Radius);


                System.out.println(Radius - Math.cos(Math.toRadians(turnRadius)) * Radius);

                //Berechnung der Y-Position
                yPosition += (Math.sin(Math.toRadians(turnRadius)) * Radius);

                System.out.println(Math.sin(Math.toRadians(turnRadius)) * Radius);
            }



        }

        while (currentDirection <= 0.00) {
            currentDirection += 360;
        }
        while (currentDirection >= 360.00) {
            currentDirection -= 360;
        }

        currentDirection = (double) Math.round(currentDirection*100)/100;

        output = xPosition + " " + yPosition + " " + currentDirection;
        System.out.println(output);
    }
}
