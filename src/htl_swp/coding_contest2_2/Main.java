package htl_swp.coding_contest2_2;

import htl_swp.maturaaufgaben.coding_contest2.Pair;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Pair> pairs = new ArrayList<>();

        String output = "";


        String input = "193 125 133 134 135 136 -52 -51 -50 -49 -48 -47 -46 -45 66 67 68 69 70 71 -38 -37 -36 -35 -34 -33 -32 -31 -30 -29 -132 -131 -130 -193 -192 -191 -190 -189 -188 -187 -186 -185 -184 -183 -182 -181 -180 -179 -178 -177 -176 -175 -174 -173 -172 -171 -170 -169 -77 -76 -75 -74 -73 -72 18 19 20 21 22 23 24 25 26 27 28 -164 -163 -65 -64 -63 -62 -61 -60 -59 -58 -57 -56 -55 -54 -53 39 40 41 42 43 44 159 160 161 162 -17 -16 -15 -14 -13 -12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 -168 -167 -166 -165 126 127 128 129 86 87 88 89 90 91 92 93 94 95 96 -124 -123 -122 -121 -120 -119 -118 -117 -116 -115 -114 -113 -112 -111 -110 -109 -108 -107 -106 -105 -104 -103 -102 -101 -100 -99 -98 -97 153 154 155 156 157 158 -148 -147 -146 -145 -144 -143 -142 -141 -140 -139 -138 -137 -85 -84 -83 -82 -81 -80 -79 -78 -152 -151 -150 -149 -45 12 44 94";
        String[] inputs = input.split(" ");
        Integer length = Integer.parseInt(inputs[0]);

        String[] PermutationString = Arrays.copyOfRange(inputs, 1, length+1);
        String[] Location = Arrays.copyOfRange(inputs, length+1,inputs.length);

        int start = Integer.parseInt(Location[1]);
        int end = Integer.parseInt(Location[3]);

        int perm1 = Integer.parseInt(Location[0]);
        int perm2 = Integer.parseInt(Location[2]);



        int[] Permutation = new int[PermutationString.length];
        for (int i = 0; i < PermutationString.length; i++) {
            Permutation[i] = Integer.parseInt(PermutationString[i]);
        }


        if (perm1 + perm2 == 1) {
            for (int i = start; i < start + (end - start) / 2; i++) {
                int temp = Permutation[i+1];

                Permutation[i+1] = -Permutation[end - i + start];
                Permutation[end - i] = -temp;
            }
        }
        if (perm1 + perm2 == -1) {
            for (int i = start + 1; i < start+(end - start) / 2; i++) {
                int temp = Permutation[i];

                Permutation[i] = -Permutation[end + start - i+1];
                Permutation[end + start - i+1] = -temp;
            }
        }

        for (int i = 0; i < Permutation.length; i++) {
            output += Permutation[i] + " ";
        }
        System.out.println(output);
    }
}

