package htl_swp.coding_contest3_MarsRover1;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String input = "2.45 90.00";
        Double outputDouble;
        String output = "";
        String[] inputs = input.split(" ");
        List<Double> numbers = new ArrayList<Double>();

        for (int i = 0; i<inputs.length; i++) {
             numbers.add(Double.parseDouble(inputs[i]));
        }

        outputDouble = numbers.get(0) / Math.sin(Math.toRadians(numbers.get(1)));
        output += outputDouble;
        System.out.println(output);
    }
}
