package htl_swp.maturaaufgaben.aufgabe1;

public class Awning_regulator implements Observer {
    @Override
    public void checkTemperature(double temperature) {
        if (temperature > 20) {
            System.out.println("Markisenregler: " + temperature + "°C!");
        }
        if (temperature < 20) {
            System.out.println("Markisenregler: Lautstärke ok!");
        }
    }
}
