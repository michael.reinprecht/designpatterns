package htl_swp.maturaaufgaben.aufgabe1;

public class Heating_controller implements Observer {
    @Override
    public void checkTemperature(double temperature) {
        if (temperature > 20) {
            System.out.println("Heizregler: " + temperature + "°C!");
        }
        if (temperature < 20) {
            System.out.println("Heizregler: Temperatur ok!");
        }
    }
}
