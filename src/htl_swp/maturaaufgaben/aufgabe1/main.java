package htl_swp.maturaaufgaben.aufgabe1;

public class main {
    public static void main(String[] args) {
        Thermometer thermometer = new Thermometer(19);

        Heating_controller heating_controller = new Heating_controller();
        Awning_regulator awning_regulator = new Awning_regulator();

        thermometer.addObserver(heating_controller);
        thermometer.addObserver(awning_regulator);

        thermometer.forceTemperatureCheck();

        thermometer.setTemperature(22);
        thermometer.forceTemperatureCheck();
    }
}
