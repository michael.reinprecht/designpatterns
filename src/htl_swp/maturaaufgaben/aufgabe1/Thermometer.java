package htl_swp.maturaaufgaben.aufgabe1;

import java.util.ArrayList;
import java.util.List;

public class Thermometer {
    private List<Observer> observers;
    private double temperature;

    public Thermometer(double temperature) {
        this.observers = new ArrayList<>();
        this.temperature = temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    public void forceTemperatureCheck() {
        for (Observer observer1 : observers) {
            observer1.checkTemperature(this.temperature);
        }

    }
}
