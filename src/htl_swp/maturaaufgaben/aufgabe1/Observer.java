package htl_swp.maturaaufgaben.aufgabe1;

public interface Observer {
    public void checkTemperature(double temperature);
}
