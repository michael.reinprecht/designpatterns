package htl_swp.maturaaufgaben.aufgabe3;

public class User {
    private String OS;

    public User(String os) {
        this.OS = os;
    }

    public void usesAlgo() {
        AlgoFactory.getAlgo(this.OS).getType();
    }
}
