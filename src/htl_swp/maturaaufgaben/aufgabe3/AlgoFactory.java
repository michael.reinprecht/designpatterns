package htl_swp.maturaaufgaben.aufgabe3;

public class AlgoFactory {
    public static Algo getAlgo(String os) {
        if (os == "Apple") {
            return (new SteveJobsAlgo());
        }
        if (os == "Linux") {
            return (new LinusTorvaldAlgo());
        }
        else {
            return (new BillGatesAlgo());
        }
      }
    }
