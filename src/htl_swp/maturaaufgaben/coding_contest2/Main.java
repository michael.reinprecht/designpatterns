package htl_swp.maturaaufgaben.coding_contest2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Pair> pairs = new ArrayList<>();

        int counter = 0;
        String output = "";


        String input = "0 3 1 6 5 -2 4 7 1 2 -2 5";
        String[] inputs = input.split(" ");
        int[] IntInputs = new int[inputs.length];

        for (int i = 0; i < inputs.length; i++) {
            IntInputs[i] = Integer.parseInt(inputs[i]);
        }

        for (int i = 0; i < IntInputs.length; i++) {
            for (int j = 0; j< IntInputs.length; j++) {

                int n1 = Math.abs(IntInputs[i]);
                int n2 = Math.abs(IntInputs[i+j]);
                if ((n1 - n2 == -1 || n1 - n2 == 1) && (IntInputs[i] <= 0 && IntInputs[i+j]>=0 || IntInputs[i] >=0 && IntInputs[i+j] <=0)) {

                    counter ++;
                    pairs.add(new Pair(IntInputs[i], IntInputs[i+j]));
                }

                if (i+j == IntInputs.length-1) {
                    j=IntInputs.length;
                }
            }
        }
        Collections.sort(pairs);


        for (int i = 0; i<pairs.size();i++) {
            output += pairs.get(i).getX() + " " + pairs.get(i).getY() + " ";
        }

        System.out.println(counter + " " + output);
    }
}
