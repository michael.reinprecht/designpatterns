package htl_swp.maturaaufgaben.coding_contest2;

public class Pair implements Comparable<Pair> {

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    private int x;
    private int y;

    @Override
    public int compareTo(Pair pair) {
        return this.x - pair.getX();
    }
}
