package htl_swp.maturaaufgaben.aufgabe2;

public class MarokkoMotor implements Motor {
    private Strategy strategy;

    public MarokkoMotor(Strategy strategy) {
        super();
        this.strategy = strategy;
    }

    @Override
    public void turnon() {
        System.out.println("This Marokko is turned on!");
        this.strategy.Injection();
    }
}