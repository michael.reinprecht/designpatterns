package htl_swp.maturaaufgaben.aufgabe2;

public class AustrianMotor implements Motor {
    private Strategy strategy;

    public AustrianMotor(Strategy strategy) {
        super();
        this.strategy = strategy;
    }

    @Override
    public void turnon() {
        System.out.println("This austrian is turned on!");
        this.strategy.Injection();
    }
}
