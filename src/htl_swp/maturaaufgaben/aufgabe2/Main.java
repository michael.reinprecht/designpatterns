package htl_swp.maturaaufgaben.aufgabe2;



public class Main {
    public static void main(String[] args) {
        Strategy strategy1 = new Affrican();
        Strategy strategy2 = new HighAlpin();

        MarokkoMotor m1 = new MarokkoMotor(strategy1);
        AustrianMotor a1 = new AustrianMotor(strategy2);

        m1.turnon();
        a1.turnon();
    }
}
