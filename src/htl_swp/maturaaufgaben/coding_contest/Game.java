package htl_swp.maturaaufgaben.coding_contest;

import htl_swp.designpatterns5bWI.z_extra.Actor;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private List<Throw> throwes;
    private int value = 0;
    private String output = "";

    public Game() {
        this.throwes = new ArrayList<>();
    }

    void addThrow(Throw t) {
        this.throwes.add(t);
    }

    void startGame(int rounds) {
        for (int i=0;i<rounds;i++) {
            if (this.throwes.get(i).getSpare() == true) {
                value += this.throwes.get(i).getValue() + this.throwes.get(i+1).getThrow1();
            }
            else if (this.throwes.get(i).getStrike() == true) {
                value += this.throwes.get(i).getValue() + this.throwes.get(i+1).getValue();
            }
            else {
                value += this.throwes.get(i).getValue();
            }
            output += value + ",";
        }
        System.out.println(output);
    }
}
