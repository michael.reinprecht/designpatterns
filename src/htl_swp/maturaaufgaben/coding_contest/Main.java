package htl_swp.maturaaufgaben.coding_contest;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String input = "3:1,2,6,4,5,2";

        String[] roundNThrows = input.split(":");
        String[] Throws = roundNThrows[1].split(",");


        Game game = new Game();

        for (int i = 0; i<Integer.parseInt(roundNThrows[0])*2;i+=2) {

            game.addThrow(new Throw(Integer.parseInt(Throws[i]), Integer.parseInt(Throws[i+1])));
            System.out.println(Integer.parseInt(Throws[i]));
            System.out.println(Integer.parseInt(Throws[i+1]));
        }

        game.startGame(Integer.parseInt(roundNThrows[0]));
    }
}
