package htl_swp.maturaaufgaben.coding_contest;

public class Throw {
    private int throw1;
    private int throw2;
    private boolean spare = false;
    private boolean strike = false;

    public Throw(int throw1, int throw2) {
        this.throw1 = throw1;
        this.throw2 = throw2;
        if (throw1 == 10) {
            this.strike = true;
        }
        if (throw1 + throw2 == 10 && this.strike == false) {
            this.spare = true;
        }
    }

    int getValue() {
        if (this.strike == true) {
            return this.throw1 + this.throw2;
        }
        if (this.strike == true) {
            return this.throw1 + this.throw2;
        }
        return this.throw1 + this.throw2;
    }

    boolean getSpare() {
        return this.spare;
    }

    boolean getStrike() {
        return this.strike;
    }

    public int getThrow1() {
        return throw1;
    }

    public int getThrow2() {
        return throw2;
    }
}
